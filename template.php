<?php

/**
 * @file
 * template.php
 */

function cultura_bootstrap_preprocess_username(&$variables) {
  $account = $variables['account'];

  $variables['extra'] = '';
  if (empty($account->uid)) {
    $variables['uid'] = 0;
    if (theme_get_setting('toggle_comment_user_verification')) {
      $variables['extra'] = ' (' . t('visitor') . ')';
    }
  }
  else {
    $variables['uid'] = (int) $account->uid;
  }

  // Set the name to a formatted name that is safe for printing and
  // that won't break tables by being too long. Keep an unshortened,
  // unsanitized version.
  $name = $variables['name_raw'] = format_username($account);
  if (drupal_strlen($name) > 30) {
    $name = drupal_substr($name, 0, 27) . '...';
  }
  $variables['name'] = filter_xss($name);

  $variables['profile_access'] = user_access('access user profiles');
  $variables['link_attributes'] = array();
  // Populate link path and attributes if appropriate.
  if ($variables['uid'] && $variables['profile_access']) {
    // We are linking to a local user.
    $variables['link_attributes'] = array('title' => t('View user profile.'));
    $variables['link_path'] = 'user/' . $variables['uid'];
  }
  elseif (!empty($account->homepage)) {
    $variables['link_path'] = $account->homepage;
    $variables['homepage'] = $account->homepage;
  }
  // We do not want the l() function to check_plain() a second time.
  $variables['link_options']['html'] = TRUE;
  // Set a default class.
  $variables['attributes_array'] = array('class' => array('username'));
}

/**
 * Implements template_preprocess_html().
 */
function cultura_bootstrap_preprocess_html(&$variables) {
  drupal_add_css('//fonts.googleapis.com/css?family=Merriweather:400,700');
  drupal_add_css('//fonts.googleapis.com/css?family=Merriweather+Sans:300');
}

/**
 * Implements template_preprocess_page().
 *
 * Add variable to page for user uploaded or (fallback) default banner.
 *
 * Make the main content area smaller for form-only pages.
 */
function cultura_bootstrap_preprocess_page(&$variables) {
  if (theme_get_setting('default_banner')) {
    global $theme_path;
    $variables['banner'] = base_path() . $theme_path . '/images/cultura-banner-1140.png';
  }
  else {
    $variables['banner'] = file_create_url(theme_get_setting('banner'));
  }

  // Make the main content area smaller for form-only pages.
  if (empty($variables['page']['sidebar_first'])
      && empty($variables['page']['sidebar_second'])
      && (arg(0) === 'customerror' || arg(0) === 'user')) {

    $variables['content_column_class'] = ' class="col-sm-6 col-sm-offset-3"';
  }
}

/**
 * Implements template_preprocess_comment().
 *
 * Remove 'Submitted by' from poster's name.
 */
function cultura_bootstrap_preprocess_comment(&$variables) {
  unset($variables['title']);

  $variables['submitted'] = t('By !username on !datetime', array('!username' => $variables['author'], '!datetime' => $variables['created']));

  // Override Drupal's unique comment link in favor of ID on node page.  This
  // means number of comments to display on a page must be set very high.
  $uri['path'] = 'node/' . $variables['node']->nid;
  $uri['options'] = array(
    'fragment' => 'comment-' . $variables['comment']->cid,
    'attributes' => array(
      'title' => t('Permalink'),
      'class' => array('permalink'),
      'rel' => 'bookmark',
    ),
  );
  $variables['permalink'] = l('#', $uri['path'], $uri['options']);
}

/**
 * Theme function implementation for bootstrap_search_form_wrapper.
 *
 * Taken from bootstrap/theme/bootstrap/bootstrap-search-form-wrapper.func.php
 *
 * Includes search glyphicon whether using the CDN or not.
 */
function cultura_bootstrap_bootstrap_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn btn-default">';
  $output .= _bootstrap_icon('search');
  $output .= '</button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}

/**
 * Overrides theme_form_element() to accept #wrapper_attributes, remove cruft.
 *
 * We are overriding the bootstrap version from form-element.func.php not the
 * one from Drupal's system module in order to preserve the addition of
 * Bootstrap classes such as help-block.
 */
function cultura_bootstrap_form_element(&$variables) {
  $element = &$variables['element'];
  $is_checkbox = FALSE;
  $is_radio = FALSE;

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  $attributes = isset($element['#wrapper_attributes']) ? $element['#wrapper_attributes'] : array();

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }

  // Check for errors and set correct error class.
  if (isset($element['#parents']) && form_get_error($element)) {
    $attributes['class'][] = 'error';
  }

  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(
        ' ' => '-',
        '_' => '-',
        '[' => '-',
        ']' => '',
      ));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if (!empty($element['#autocomplete_path']) && drupal_valid_path($element['#autocomplete_path'])) {
    $attributes['class'][] = 'form-autocomplete';
  }
  $attributes['class'][] = 'form-item';

  // See http://getbootstrap.com/css/#forms-controls.
  if (isset($element['#type'])) {
    if ($element['#type'] == "radio") {
      $attributes['class'][] = 'radio';
      $is_radio = TRUE;
    }
    elseif ($element['#type'] == "checkbox") {
      $attributes['class'][] = 'checkbox';
      $is_checkbox = TRUE;
    }
    else {
      $attributes['class'][] = 'form-group';
    }
  }

  $description = FALSE;
  $tooltip = FALSE;
  // Convert some descriptions to tooltips.
  // @see bootstrap_tooltip_descriptions setting in _bootstrap_settings_form()
  if (!empty($element['#description'])) {
    $description = $element['#description'];
    if (theme_get_setting('bootstrap_tooltip_enabled') && theme_get_setting('bootstrap_tooltip_descriptions') && $description === strip_tags($description) && strlen($description) <= 200) {
      $tooltip = TRUE;
      $attributes['data-toggle'] = 'tooltip';
      $attributes['title'] = $description;
    }
  }

  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }

  $prefix = '';
  $suffix = '';
  if (isset($element['#field_prefix']) || isset($element['#field_suffix'])) {
    // Determine if "#input_group" was specified.
    if (!empty($element['#input_group'])) {
      $prefix .= '<div class="input-group">';
      $prefix .= isset($element['#field_prefix']) ? '<span class="input-group-addon">' . $element['#field_prefix'] . '</span>' : '';
      $suffix .= isset($element['#field_suffix']) ? '<span class="input-group-addon">' . $element['#field_suffix'] . '</span>' : '';
      $suffix .= '</div>';
    }
    else {
      $prefix .= isset($element['#field_prefix']) ? $element['#field_prefix'] : '';
      $suffix .= isset($element['#field_suffix']) ? $element['#field_suffix'] : '';
    }
  }

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      if ($is_radio || $is_checkbox) {
        $output .= ' ' . $prefix . $element['#children'] . $suffix;
      }
      else {
        $variables['#children'] = ' ' . $prefix . $element['#children'] . $suffix;
      }
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if ($description && !$tooltip) {
    $output .= '<p class="help-block">' . $element['#description'] . "</p>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Overrides theme_double_field().
 */
function cultura_bootstrap_double_field($vars) {
  $element = $vars['element'];
  $output = '<div class="col-xs-6">' . $element['#item']['first'] . '</div>';
  $output .= '<div class="col-xs-6">' . $element['#item']['second'] . '</div>';
  return $output;
}

/**
 * Overrides theme_field() for cultura_discussion_answers on cultura_discussion.
 */
function cultura_bootstrap_field__cultura_discussion_answers__cultura_discussion($variables) {
  $item = $variables['items'][0];
  $output = '<div class="row">' . drupal_render($item) . '</div>';
  return $output;
}

/**
 * Overrides theme_field() for cultura_questionnaire_prompts on cultura_discussion.
 */
function cultura_bootstrap_field__cultura_questionnaire_prompts__cultura_discussion($variables) {
  $item = $variables['items'][0];
  $output = '<div class="row">' . drupal_render($item) . '</div>';
  return $output;
}
