<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for Bootstrap based themes when admin theme is not.
 *
 * @see theme/settings.inc
 */


/**
 * Implements hook_form_FORM_ID_alter().
 */
function cultura_bootstrap_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  $key = 'cultura_bootstrap';

  $form['banner'] = array(
    '#type' => 'fieldset',
    '#title' => t('Banner image settings'),
  );
  $form['banner']['default_banner'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default banner'),
    '#default_value' => theme_get_setting('default_banner', $key),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the banner supplied with it.')
  );

  $form['banner']['settings']['banner_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom banner'),
    '#description' => t('The path to the file you would like to use as your banner file instead of the default banner.'),
    '#default_value' => theme_get_setting('banner_path', $key),
  );
  $form['banner']['settings']['banner_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload banner image'),
    '#maxlength' => 40,
    '#description' => t("If you don't have direct file access to the server, use this field to upload your banner.")
  );

  // Place in vertical tabs Bootstrap base theme provides for settings.
  $form['banner']['#group'] = 'general';

  // Add our validation function.
  $form['#validate'][] = 'cultura_bootstrap_form_system_theme_settings_validate';
}


/**
 * Validate banner upload or file path and add to form value for saving.
 */
function cultura_bootstrap_form_system_theme_settings_validate($form, &$form_state) {

  // Handle file uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded banner.
  $file = file_save_upload('banner_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['banner_upload'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('banner_upload', t('The logo could not be uploaded.'));
    }
  }


  // Code borrowed from function system_theme_settings_submit() but done in
  // validate so that function can take care of saving the theme configuration
  // all together in one horrifying array in the variables table.

  $values =& $form_state['values'];

  // If the user uploaded a new banner, save it to a permanent location
  // and use it in place of the default theme-provided file.
  if ($file = $values['banner_upload']) {
    unset($values['banner_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['banner'] = $filename;
  }
  // If the user entered a path relative to the system files directory for
  // a banner, store a public:// URI so the theme system can handle it.
  else if (!empty($values['banner_path'])) {
    $values['banner'] = _system_theme_settings_validate_path($values['banner_path']);
  }

}